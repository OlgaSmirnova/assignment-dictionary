global print_char
global print_newline
global print_string
global exit
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy
global string_length

section .text

 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rax + rdi], 0
    je .end
    inc rax
    jmp .loop
.end:        
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    push rsi
    call string_length
    mov rdx, rax
    pop rdi
    pop rsi
    mov rax, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov rdx, 1
    mov rax, 1
    push rdi
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    print_uint:
    push 0          ; null terminated string
    mov rbx, 10     ; devident
    mov rax, rdi    ; put in rax for calculation
    add rsp, 7
.count:
    xor rdx, rdx
    div rbx         ; rax div rcx -% rax
    add dl, '0'     ; one lowest byte
    mov dh, byte [rsp]
    add rsp, 1
    push dx
    test rax, rax
    jne .count
    mov rdi, rsp
    call print_string
    mov rdi, rsp
    call string_length
    add rsp, rax
    inc rsp         ; jump over null-terminator
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rbx
    mov rbx, rdi
    cmp rbx, 0
    jns .count
    mov rdi, '-'
    call print_char
    mov rdi, rbx
    neg rdi
.count:
    call print_uint
    pop rbx
    ret
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rdx, rdx
.loop:
    mov dl, byte [rsi]
    mov al, byte [rdi]
    inc rdi
    inc rsi
    cmp dl, al
    jne .not_null
    cmp al, 0
    jne .loop
    mov rax, 1
    ret
.not_null:
    mov rax, 0
    ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret  

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx 
.loop:
    push rcx
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop rcx
    cmp rax, 0x20
    je .first_symbol
    cmp rax, 0x9
    je .first_symbol
    cmp rax, 0xA
    je .first_symbol
    cmp rax, 0
    je .end
    mov [rdi + rcx], rax
    inc rcx
    cmp rcx, rsi
    jge .err
    jmp .loop
.first_symbol:
	cmp rcx, 0
	je .loop
	jmp .end
.err:
	xor rax, rax
	xor rdx, rdx
.end:
	xor rax, rax
	mov [rdi + rcx], rax
	mov rax, rdi
	mov rdx, rcx
	ret    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx 
    push rbx
    mov r9, 10
.loop:
    mov bl, byte [rdi]
    test bl, bl
    jz .finish
    cmp bl, 0x30
    jb .finish
    cmp bl, 0x39
    ja .finish
    mul r9
    and bl, 0xF
    add rax, rbx
    inc rdi
    inc rcx
    jmp .loop
.finish:
    pop rbx
    mov rdx, rcx
    ret






; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    push rbx
    xor rbx, rbx
    mov bl, byte [rdi]
    cmp bl, '-'
    je .neg_symbol
    call parse_uint
    pop rbx
    ret
.neg_symbol:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    pop rbx
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length  
    cmp rdx, rax
    jb .else
    mov rcx, rax
.loop:
    test rcx, rcx
    je .end
    dec rcx              
    mov dl, byte [rdi + rcx]   
    mov byte [rsi + rcx], dl
    jmp .loop
.end:
    mov byte [rsi + rax], 0
    ret
.else:
    mov rax, 0
    ret
