section .text
global find_word

extern string_length
extern string_equals

find_word:
.loop:
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    cmp rax, 0 
    jne .found 
    mov rsi, [rsi]
    test rsi, rsi
    jz .not_found
    jmp .loop
.not_found:
    xor rax, rax
    ret
.found:
    add rsi, 8
    push rsi
    call string_length
    pop rsi
    add rax, rsi
    inc rax
    ret
